FROM busybox:1.32.0
# Build-time metadata as defined at http://label-schema.org
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION=1.0.0
LABEL org.label-schema.build-date=$BUILD_DATE \
        org.label-schema.name="TEST_SEMVER" \
        org.label-schema.description="Example project description in 300 chars or less" \
        org.label-schema.vendor="GJOSA" \
        org.label-schema.version=$VERSION \
        org.label-schema.schema-version="1.0"

 COPY /files /tmp/goose
